Icon: Avalanche by Andi Nur Abdillah from the Noun Project

Avalance
========
Avalanche allows you to automatically spin up infrastructure to perform
load tests. It works using AWS CloudFormation to give you a GUI to
control the test infrastructure, the rest is automated for you.

Preparation
-----------
1.  Clone the repository code, or download the avalanche.yaml file.

2.  Log into your AWS account.

3.  Create an S3 bucket for your JMX test files & associated data, you
    can optionally create a second bucket for results to be exported to,
    or use the same one with a different folder as shown in the
    screenshot:

    ![](./media/image1.png)

    ![](./media/image2.png)

4.  Upload your JMX files to the folder you want to contain your source
    data.

    ![](./media/image3.png)

Running Avalanche
-----------------
1.  Go to CloudFormation in the AWS console.

    ![](./media/image4.png)

2.  Click "Create stack"

    ![](./media/image5.png)

3.  Keep it set to "Template is ready" and select "Upload a template
    file", then upload the avalanche.yaml file and click "Next"

    ![](./media/image6.png)

4.  Enter a name for your stack (i.e. "WebLoadTest").

5.  For "Instance configuration" select the VPC to run in.

6.  More importantly, select a "public" subnet, i.e. a subnet that you
    know has internet access. You can select multiple subnets to spread
    the servers around to get the most available capacity. The subnets
    must exist inside the VPC you selected.

    ![](./media/image7.png)

7.  Enter the number of instances you want, each instance will run an
    identical JMX file so if you have 1000 test threads then 3 servers
    would make it 3000 threads.

8.  Select an instance type, larger instances have more network
    bandwidth, RAM, and CPU time for larger test runs. But they cost
    more.

9.  For "Load test configuration" you need to reference the bucket &
    folders you created before, be sure to type in the name of the JMX
    file you want to run as well:

    ![](./media/image8.png)

10. When you're happy, click "Next". Remember that the buckets & folder
    have to exist or it will fail.

11. Skip past the  "Configure stack options" screen by pressing "Next".

12. On the final "Review" page, scroll down and check the IAM
    acknowledgement. This is so Avalanche can set its own permissions.

    ![](./media/image9.png)

13. Your stack will start building, this can take up to 5 minutes to
    provision everything where the stack will change from
    "CREATE\_IN\_PROGRESS" to "CREATE\_COMPLETE". You can select
    "Events" or "Resources" to see the state of the stack:

    ![](./media/image10.png)

Stopping a test
---------------
1.  In CloudFormation, select the load test stack and click the "Delete"
    button:

    ![](./media/image11.png)

2.  Confirm the deletion

    ![](./media/image12.png)

Monitoring
----------
You can go to EC2 and see your Avalanche instances running after the
stack is done:

![](./media/image13.png)

If the test is running OK then on the monitoring tab you will see CPU
usage and network bandwidth being used up shortly after boot time.

If you feel Linux savvy you can also use SSM Session Manager to remotely
connect to them:

1.  Go to SSM.

    ![](./media/image14.png)

2.  Select "Session Manager"

    ![](./media/image15.png)

3.  Select "Start session"

    ![](./media/image16.png)

4.  Select an Avalanche Agent

    ![](./media/image17.png)

5.  Click "Start session" to get a comforting black & white terminal.

    ![](./media/image18.png)

    ![](./media/image19.png)

    *cosy*

6.  Execute a command like "top" to see if "java" is the \#1 process

    ![](./media/image20.png)

    ... or check Dynatrace/NewRelic to see if your service is indeed
    getting hammered!

    ![](./media/image21.png)

Improvements that we are working on
-----------------------------------
-   Shipping the logs to CloudWatch so you can monitor for errors or
    JMeter issues without remoting on.

-   Self-destructing test runs; you currently have to manually destroy
    the stack to stop the test run.

-   SNS alerts that will email or text you when a test run officially
    starts or stops.

-   A GUI that wraps this up into an even neater package or command line
    tool.